#!/usr/bin/env -S deno run --allow-read --allow-write
// NOTE: must be run inside 2023/ directory
import 'better_type'
import { logger } from 'util'

const file = await Deno.readTextFile('day-02-CubeConundrum.txt')
const log = logger('day-02-CubeConundrum.log')
const input_list = file.split('\n')

interface iCubeBag {
  red: number
  green: number
  blue: number
}
class CubeBag implements iCubeBag {
  constructor(
    public red: number = 0,
    public green: number = 0,
    public blue: number = 0,
  ) {}

  is_contained(bag: CubeBag): boolean {
    const same_red = this.red >= bag.red
    const same_green = this.green >= bag.green
    const same_blue = this.blue >= bag.blue

    return same_red && same_green && same_blue
  }

  calc_max_lim(bag: CubeBag): CubeBag {
    const max_red = this.red >= bag.red ? this.red : bag.red
    const max_green = this.green >= bag.green ? this.green : bag.green
    const max_blue = this.blue >= bag.blue ? this.blue : bag.blue

    return new CubeBag(max_red, max_green, max_blue)
  }
}

class Game {
  id: number
  bag_ls: CubeBag[]

  static #cube_type_order = ['red', 'green', 'blue']

  constructor(raw: string) {
    const [id, bag_ls] = this.#parse_game(raw)
    this.id = id
    this.bag_ls = bag_ls
  }

  #parse_cube(carrier: number[], cube_str: string): number[] {
    const [is_match, count, type] = cube_str.match(/(\d+) (\w+)/i) ?? []

    if (!is_match) return carrier

    const order = Game.#cube_type_order.indexOf(type.toLowerCase())
    carrier[order] = parseInt(count)

    return carrier
  }

  #parse_set(set_str: string): CubeBag {
    const set_game = set_str.split(',').reduce(
      (l, c) => this.#parse_cube(l, c),
      [] as number[],
    )
    return new CubeBag(...set_game)
  }

  #parse_game(raw: string): [number, CubeBag[]] {
    const [_, id, content] = raw.match(/^Game (\d+)\: (.+)/i) ?? []

    if (!id) throw new Error(`Unable to parse the input: ${raw}`)

    const set_ls = content.split(';').map(this.#parse_set, this)
    return [parseInt(id), set_ls]
  }

  meet_ideal(ideal_bag: CubeBag): boolean {
    const possible_set = this.bag_ls.filter(ideal_bag.is_contained, ideal_bag)
    return possible_set.length === this.bag_ls.length
  }

  get_max_sz(): CubeBag {
    return this.bag_ls.reduce((minbag, bag) => minbag.calc_max_lim(bag))
  }
}

/** Part I */
const ideal_bag = new CubeBag(12, 13, 14)
function solution_p1(record: string): [boolean, number] {
  const game = new Game(record)
  return [game.meet_ideal(ideal_bag), game.id]
}

/** Part II */
function solution_p2(record: string): number {
  const game = new Game(record)
  const bag = game.get_max_sz()
  return bag.red * bag.green * bag.blue
}

/** Main */
let result_p1 = 0, result_p2 = 0
for (const input of input_list) {
  const [p1_match, p1_id] = solution_p1(input)
  p1_match && (result_p1 += p1_id)
  result_p2 += solution_p2(input)
}
console.log(result_p1, result_p2)
