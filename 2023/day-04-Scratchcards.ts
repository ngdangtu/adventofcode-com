#!/usr/bin/env -S deno run --allow-read --allow-write
// NOTE: must be run inside 2023/ directory
import 'better_type'
import { logger } from 'util'

const file = await Deno.readTextFile('day-04-Scratchcards.txt')
const log = logger('day-04-Scratchcards.log')
const pile_of_card = file.split('\n')

const match_ls: number[] = []
match_ls.length = pile_of_card.length
match_ls.fill(0)

/** Part I */
function solution_p1(
  inx: number,
  win_ls: string[],
  own_ls: string[],
): number {
  const calc_score = (score: number, val: string) => {
    if (!win_ls.includes(val)) return score
    match_ls[inx] += 1 // for p2
    return score === 0 ? 1 : score * 2
  }
  return own_ls.reduce(calc_score, 0)
}

/** Part II */
const copies_ls: number[] = []
copies_ls.length = pile_of_card.length
copies_ls.fill(1)
function solution_p2(inx: number) {
  const copies_count = match_ls[inx]

  let count = copies_count
  while (count > 0) {
    const next_inx = inx + 1 + copies_count - count
    const next_val = copies_ls[next_inx] === 0 ? 1 : copies_ls[inx]
    copies_ls[next_inx] += next_val
    count--
  }
}

/** Helper */
function parse_card(raw: string): [number, string[], string[]] {
  const [_, card_inx, win_num, own_num] = raw
    .replace(/\s+/g, ' ')
    .match(/Card\s+(\d+)\:\s(.+)\s\|\s(.+)/i)!

  return [
    parseInt(card_inx) - 1, // *_ls arrays start from 0
    win_num.split(' '),
    own_num.split(' '),
  ]
}

/** Main */
let result_p1 = 0, result_p2 = 0
for (const card of pile_of_card) {
  if (!card) continue
  const [inx, win_ls, own_ls] = parse_card(card)
  result_p1 += solution_p1(inx, win_ls, own_ls)
  solution_p2(inx) // update copies ls
}
result_p2 += copies_ls.reduce((sum, copies) => sum + copies, 0)
console.log(result_p1)
console.log(result_p2)
