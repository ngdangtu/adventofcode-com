#!/usr/bin/env -S deno run --allow-read --allow-write
// NOTE: must be run inside 2023/ directory
import 'better_type'
import { logger } from 'util'

const file = await Deno.readTextFile('day-01-Trebuchet.txt')
const log = logger('day-01-Trebuchet.log')
const input_list = file.split('\n')

/** Part I */
type Digit = { ten?: number; one?: number }
function solution_p1(raw: string): number {
  if (!raw) return 0
  const digit: Digit = {}

  for (const letter of raw) {
    if (isNaN(letter)) continue
    const number = parseInt(letter)
    if (!digit.ten) {
      digit.ten = number
      continue
    }
    digit.one = number
  }

  return digit.ten! * 10 + (digit.one || digit.ten!)
}

/** Part II * working */
function solution_p2(raw: string): number {
  if (!raw) return 0

  let cursor = 0, fmtstr = raw
  const update_fmtstr = (newstr: string) => {
    if (fmtstr === newstr) {
      cursor++
    } else {
      fmtstr = newstr
      cursor = 0
    }
  }

  while (cursor < fmtstr.length) {
    const letter = fmtstr[cursor]
    log(cursor, letter, fmtstr)

    switch (letter) {
      case 'e':
        update_fmtstr(
          fmtstr.replace('eight', '8'),
        )
        break

      case 'o':
        update_fmtstr(
          fmtstr.replace('one', '1'),
        )
        break

      case 't':
        update_fmtstr(
          fmtstr.replace('two', '2').replace('three', '3'),
        )
        break

      case 'f':
        update_fmtstr(
          fmtstr.replace('four', '4').replace('five', '5'),
        )
        break

      case 's':
        update_fmtstr(
          fmtstr.replace('six', '6').replace('seven', '7'),
        )
        break

      case 'n':
        update_fmtstr(
          fmtstr.replace('nine', '9'),
        )
        break

      default:
        cursor++
    }
  }
  log(cursor, fmtstr, '\n')

  return solution_p1(fmtstr)
}

/** Main */
let result_p1 = 0, result_p2 = 0
for (const input of input_list) {
  result_p1 += solution_p1(input)
  result_p2 += solution_p2(input)
}
console.log(result_p1)
console.log(result_p2)
