#!/usr/bin/env -S deno run --allow-read --allow-write
// NOTE: must be run inside 2023/ directory
import 'better_type'
import { logger } from 'util'

const log = logger('day-06-WaitForIt.log')

const record_list: Record[] = [
  { time: 48, distance: 296 },
  { time: 93, distance: 1928 },
  { time: 85, distance: 1236 },
  { time: 95, distance: 1391 },
]

/** Part I */
/**
 * let t = time,
 *     d = distance,
 *     x = hold time,
 *     l = time left,
 *     c = time cost to got expected distance;
 *
 * c = d
 * c = x * l
 * c = x * (t - x)
 * c = tx - x² = d
 *
 * ⇔ -x² + tx =  d
 * ⇔  x² - tx = -d
 * ⇔  x² - tx + ½t² = -d + ½t²
 * ⇔ (x - ½t)² = (-4d + t²)/4
 * ⇔ (x - ½t)² = (t² -4d)/4
 * ⇔  x - t/2 = ± √(t² -4d)/2
 * ⇔  x = (t ± √(t² -4d))/2
 *
 * https://vi.wikipedia.org/wiki/Phương_trình_bậc_hai
 * https://review.edu.vn/hoc-thuat/7-hang-dang-thuc-dang-nho
 * https://www.mathsisfun.com/algebra/polynomials.html
 */
function solution_p1({ time, distance }: Record): number {
  const t = time
  const d = distance + 1

  // nevermind name, I gave up on make a meaningful name for Math.
  const delta = Math.sqrt(t * t - 4 * d)
  if (Number.isNaN(delta)) return 0

  const max_hold = (t + delta) / 2
  const min_hold = (t - delta) / 2
  return Math.floor(max_hold) - Math.ceil(min_hold - 1)
}

/** Part II */
function solution_p2(ls: Record[]): number {
  let t = '', d = ''

  for (const { time, distance } of ls) {
    t += time
    d += distance
  }

  return solution_p1({ time: parseInt(t), distance: parseInt(d) })
}

/** Helpers */
interface Record {
  time: number
  distance: number
}

/** Main */
let result_p1 = 1
const result_p2 = solution_p2(record_list)
for (const record of record_list) {
  result_p1 *= solution_p1(record)
}
console.log(result_p1)
console.log(result_p2)

// console.log(solution_p1({ time: 7, distance: 9 }))
// console.log(solution_p1({ time: 15, distance: 40 }))
// console.log(solution_p1({ time: 30, distance: 200 }))
