#!/usr/bin/env -S deno run --allow-read --allow-write
// NOTE: must be run inside 2023/ directory
import 'better_type'
import { logger } from 'util'

const symbol = '!'

const [engine_map, engine_sz] = fmt_raw(
  await Deno.readTextFile('day-03-GearRatios.txt'),
)
const log = logger('day-03-GearRatios.log')

interface Cursor {
  row: number
  col: number
}
const cursor: Cursor = { row: 0, col: 0 }

// For debugging
const all_valid_digit: number[] = []

// In case the input has overlap symbol
// interface Token {
//   count: number
//   length: number
// }
// const scanned_map: Array<(Token | 0)[] | 0> = []

/** Part I */
function look_around_cube({ col: c, row: r }: Cursor): number {
  // look around in 1-char radius

  const look_digit = (
    row: string,
    step: -1 | 1,
    char: string,
    col: number,
    val = '',
  ): number => {
    if (!char || ['.', symbol].includes(char)) return val ? parseInt(val) : 0
    const next_col = col + step
    return look_digit(row, step, row[next_col], next_col, val + char)
  }

  const row_ls = [engine_map[r]]
  r > 0 && row_ls.push(engine_map[r - 1])
  r < engine_sz.h && engine_map[r + 1]

  return row_ls
    .reduce(
      (sum, row) => {
        // search left col
        const d1 = look_digit(row, -1, row[c], c)
        sum += d1
        all_valid_digit.push(d1)

        // search middle first, if not found, search right col
        const d2 = look_digit(row, 1, row[c], c) ||
          look_digit(row, 1, row[c + 1], c + 1)
        sum += d2
        all_valid_digit.push(d2)

        return sum
      },
      0,
    )
}
function solution_p1(row: string, cursor: Cursor): number {
  let sum_of_row = 0
  for (let col = 0; col < row.length; col++) {
    if (row[col] != symbol) continue
    cursor.col = col
    sum_of_row += look_around_cube(cursor)
  }
  return sum_of_row
}

/** Part II * working */
// function solution_p2(raw: string): number {
//   //
// }

/** Helper */
function fmt_raw(raw: string): [string[], { w: number; h: number }] {
  // credit to Efuziom
  const fmt_data = raw.replaceAll(/[\/|\@|\*|\$|\=|\&|\#|\-|\+|\%]+/g, symbol)
  const lines = fmt_data.split('\n')
  return [lines, { w: lines[0].length, h: lines.length }]
}

let result_p1 = 0
for (let row = 0; row < engine_sz.h; row++) {
  const row_map = engine_map[row]
  cursor.row = row

  result_p1 += solution_p1(row_map, cursor)
}
console.log(result_p1)
log(all_valid_digit.join('\n'))
