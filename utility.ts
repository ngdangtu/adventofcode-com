export function logger(filepath: string) {
  const opt: Deno.WriteFileOptions = {
    append: true,
    mode: 0o644,
  }
  return (...content: Array<string | number | boolean>) =>
    Deno.writeTextFileSync(filepath, content.join(', ') + '\n', opt)
}
